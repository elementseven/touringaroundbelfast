<p class="mt-8 text-center text-xs text-80">
    <a href="https://touringaroundbelfast.com" class="text-primary dim no-underline">Touring Around Belfast</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Element Seven Digital Limited
    <span class="px-1">&middot;</span>
    v{{ \Laravel\Nova\Nova::version() }}
</p>
