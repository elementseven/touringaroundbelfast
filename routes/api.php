<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/categories/store', 'CategoryController@store')->name('categories-store');
Route::get('/categories/get/{limit}', 'CategoryController@getCategories')->name('categories-get');
Route::get('/categories/del/{category}', 'CategoryController@destroy')->name('categories-destroy');

Route::get('/news/get', 'PageController@getPosts')->name('get-posts');
Route::post('/news/store', 'PostController@store')->name('news-store');
Route::post('/news/update/{post}', 'PostController@update')->name('news-update');
Route::get('/news/get-posts/{category}/{limit}', 'PostController@getPosts')->name('get-posts');
Route::get('/news/del/{post}', 'PostController@destroy')->name('news-destroy');
Route::post('/news/image-upload', 'PostController@imageUpload')->name('news-image-upload');


Route::get('/tours/home-get-list', 'PageController@getToursList')->name('get-tours-list');
Route::get('/tours/get', 'TourController@getTours')->name('get-tours');
Route::post('/tours/store', 'TourController@store')->name('tours-store');
Route::post('/tours/update/{tour}', 'TourController@update')->name('tours-update');
Route::get('/tours/get-tours/{category}/{limit}', 'TourController@getTours')->name('get-tours');
Route::get('/tours/del/{tour}', 'TourController@destroy')->name('tours-destroy');
Route::post('/tours/image-upload', 'TourController@imageUpload')->name('tours-image-upload');

Route::post('/pictures/store/{tour}', 'PictureController@store')->name('pictures-store');
Route::post('/pictures/del/{picture}', 'PictureController@destroy')->name('pictures-destroy');
Route::post('/pictures/update/{tour}', 'PictureController@update')->name('pictures-update');

Route::get('/events/get/{tour}', 'EventController@get')->name('get-events');
Route::get('/events/by-category/{category}', 'EventController@getByCategory')->name('get-events-by-category');
Route::post('/events/store', 'EventController@store')->name('event-store');
Route::post('/events/del/{event}', 'EventController@destroy')->name('event-destroy');

Route::get('/analytics/lastweek', 'AnalyticsController@lastweek')->name('analytics-lastweek');
Route::get('/analytics/user-type', 'AnalyticsController@userTypes')->name('analytics-user-types');
Route::get('/analytics/top-referrers', 'AnalyticsController@topReferrers')->name('analytics-top-referrers');
Route::get('/analytics/popular-pages', 'AnalyticsController@popularPages')->name('analytics-popular-pages');