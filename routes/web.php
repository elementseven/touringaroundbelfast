<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PageController@welcome')->name('welcome');
Route::get('/about', 'PageController@about')->name('about');
Route::get('/tours', 'PageController@tours')->name('tours');
Route::get('/tours/{slug}', 'PageController@tourShow')->name('tours-show');
Route::get('/testimonials', 'PageController@testimonials')->name('testimonials');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/tandcs', 'PageController@tandcs')->name('tandcs');
Route::get('/privacy-policy', 'PageController@privacyPolicy')->name('privacy-policy');
Route::post('/send-message', 'SendMail@enquiry')->name('send-message');
Route::post('/request-tour', 'SendMail@tour')->name('request-tour');
Auth::routes();

Route::get('/logout', function(){
	Auth::logout();
	return redirect('/login');
})->name('home');