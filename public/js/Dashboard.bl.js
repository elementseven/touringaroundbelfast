(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Dashboard"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/users/views/Dashboard.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/users/views/Dashboard.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
var dashboardHeader = function dashboardHeader() {
  return __webpack_require__.e(/*! import() | dashboardHeader */ "dashboardHeader").then(__webpack_require__.bind(null, /*! ../components/dashboard/Header.vue */ "./resources/js/users/components/dashboard/Header.vue"));
};

var dashboardSection1 = function dashboardSection1() {
  return __webpack_require__.e(/*! import() | dashboardSection1 */ "dashboardSection1").then(__webpack_require__.bind(null, /*! ../components/dashboard/Section1.vue */ "./resources/js/users/components/dashboard/Section1.vue"));
};

var dashboardSection2 = function dashboardSection2() {
  return __webpack_require__.e(/*! import() | dashboardSection2 */ "dashboardSection2").then(__webpack_require__.bind(null, /*! ../components/dashboard/Section2.vue */ "./resources/js/users/components/dashboard/Section2.vue"));
};

var dashboardSection3 = function dashboardSection3() {
  return __webpack_require__.e(/*! import() | dashboardSection3 */ "dashboardSection3").then(__webpack_require__.bind(null, /*! ../components/dashboard/Section3.vue */ "./resources/js/users/components/dashboard/Section3.vue"));
};

var dashboardSection4 = function dashboardSection4() {
  return __webpack_require__.e(/*! import() | dashboardSection4 */ "dashboardSection4").then(__webpack_require__.bind(null, /*! ../components/dashboard/Section4.vue */ "./resources/js/users/components/dashboard/Section4.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      breadcrumbs: [{
        href: '/customers/dashboard',
        link: true,
        text: 'Dashboard'
      }]
    };
  },
  components: {
    dashboardHeader: dashboardHeader,
    dashboardSection1: dashboardSection1,
    dashboardSection2: dashboardSection2,
    dashboardSection3: dashboardSection3,
    dashboardSection4: dashboardSection4
  },
  mounted: function mounted() {
    this.$emit('pageChange', {
      'type': 'dark',
      'page': 'home',
      'breadcrumbs': this.breadcrumbs
    });
  },
  metaInfo: {
    title: 'Dashboard - Xeroplas',
    meta: [{
      name: 'description',
      content: 'Xeroplas customer dashboard'
    }, {
      name: 'og-image',
      content: 'https://xeroplas.com/img/og.jpg'
    }]
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/users/views/Dashboard.vue?vue&type=template&id=2cda8704&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/users/views/Dashboard.vue?vue&type=template&id=2cda8704& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c(
      "div",
      { staticClass: "row" },
      [
        _c("dashboard-header"),
        _vm._v(" "),
        _c("dashboard-section-1"),
        _vm._v(" "),
        _c("dashboard-section-2"),
        _vm._v(" "),
        _c("dashboard-section-3"),
        _vm._v(" "),
        _c("dashboard-section-4")
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/users/views/Dashboard.vue":
/*!************************************************!*\
  !*** ./resources/js/users/views/Dashboard.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_2cda8704___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=2cda8704& */ "./resources/js/users/views/Dashboard.vue?vue&type=template&id=2cda8704&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/js/users/views/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_2cda8704___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_2cda8704___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/users/views/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/users/views/Dashboard.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/users/views/Dashboard.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/users/views/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/users/views/Dashboard.vue?vue&type=template&id=2cda8704&":
/*!*******************************************************************************!*\
  !*** ./resources/js/users/views/Dashboard.vue?vue&type=template&id=2cda8704& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_2cda8704___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=2cda8704& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/users/views/Dashboard.vue?vue&type=template&id=2cda8704&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_2cda8704___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_2cda8704___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);