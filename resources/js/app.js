/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.LazyLoad = require('vanilla-lazyload');
window.Vue = require('vue');
window.moment = require('moment');
require('./plugins/modernizr-custom.js');
require('./plugins/cookieConsent.js');
require('waypoints/lib/jquery.waypoints.min.js');
require('./a-general.js');
require("babel-polyfill");
import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);
import VuePhoneNumberInput from 'vue-phone-number-input';
import 'vue-phone-number-input/dist/vue-phone-number-input.css';
import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';
import Sticky from 'vue-sticky-directive';
Vue.use(Sticky);

Vue.component('datetime', Datetime);
Vue.use(VuePhoneNumberInput);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component('vue-phone-number-input', VuePhoneNumberInput);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('news-index', require('./components/news/Index.vue').default);
Vue.component('news-show', require('./components/news/Show.vue').default);
Vue.component('opening-hours', require('./components/OpeningHours.vue').default);

Vue.component('contact-page-form', require('./components/ContactPageForm.vue').default);
Vue.component('tour-form', require('./components/TourForm.vue').default);
Vue.component('follow-box', require('./components/FollowBox.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
