@php
$page = 'Privacy Policy';
$pagetitle = 'Privacy Policy | Touring Around Belfast';
$metadescription = 'We take your privacy and data security very seriously. Please read our Privacy Policy.';
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://touringaroundbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5">
    <div class="row pt-5 mob-pt-0">
        <div class="col-lg-11 position-relative z-2">
            <h1 class="mw-100">Privacy Policy</h1>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-12 mb-5 pb-5">
          
        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')
@endsection