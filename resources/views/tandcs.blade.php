
@php
$page = 'Terms & Conditions';
$pagetitle = 'Terms & Conditions | Touring Around Belfast';
$metadescription = 'Our Terms & Conditions are always available to read on our website.';
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://touringaroundbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5 mt-5">
    <div class="row mob-pt-0">
        <div class="col-lg-11 position-relative z-2">
            <h1 class="mb-5 mw-100">Terms &amp; Conditions</h1>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container pb-5">
    <div class="row justify-content-center">
        <div class="col-lg-12 mb-5 pb-5">

        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')
@endsection