@php
$page = 'Testimonials';
$pagetitle = 'Testimonials | Touring Around Belfast';
$metadescription = 'Testimonials';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://touringaroundbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5 my-5 mob-mb-0">
  <div class="row pt-5 mob-pt-0 justify-content-center">
    <div class="col-lg-8 pt-5 text-center">
      <h1 class="mw-100">Testimonials</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mb-5 pb-5">
	@foreach($testimonials as $t)
  <div class="row justify-content-center">
    <div class="col-lg-10">
      <div class="testimonial text-center py-5">
        <img srcset="/img/icons/quote-top.svg" alt="Touring Around Belfast - Quote mark top" class="lazy mb-4" width="100" />
        <p class="testimonial-text">{{$t->content}}</p>
        <p class="testimonial-title"><b>{{$t->name}} - {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $t->date)->format('jS F Y')}}</b></p>
        @if($t->link_url)
        <p><a href="{{$t->link_url}}" target="_blank"><i class="fa fa-globe"></i> <b>{{$t->link_text}}</b></a></p>
        @endif
      </div>
    </div>
  </div>
  @endforeach
</div>
@endsection