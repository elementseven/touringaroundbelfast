@php
$page = 'Tours';
$pagetitle = 'Tours | Touring Around Belfast';
$metadescription = 'Touring Around Belfast.';
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://touringaroundbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'og_image' => $ogimage])
@section('header')
<header class="container-fluid position-relative bg py-5 tours-top bg-down-up">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container z-2 position-relative py-5">
      <div class="row py-5 mob-py-0">
        <div class="col-12 pt-5 mob-py-0 mob-px-4">
          <div class="row">
            <div class="col-xl-9 text-white pb-5">
              <h1 class="text-white mb-3">Tours</h1>
              <p class="text-white text-large mb-4"><b>Please review our current tours and if there is anything you would like to see or learn about then let us know and we be happy to arrange an itinerary specially designed for you.</b></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container container-wide px-5 mob-px-3 py-5">
  <div class="row mt-minus-small z-2 position-relative">
    @foreach($tours as $tour)
    <div class="col-lg-4 mb-5 mob-px-4">
      
        <div class="card border-0 shadow overflow-hidden tour-box text-center text-md-left text-dark">
          <div class="tour-image">
            <picture> 
              <source media="(min-width: 900px)" srcset="{{$tour->getFirstMediaUrl('tours', 'normal')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <source media="(min-width: 601px)" srcset="{{$tour->getFirstMediaUrl('tours', 'normal-webp')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <source srcset="{{$tour->getFirstMediaUrl('tours', 'normal-webp')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <img srcset="{{$tour->getFirstMediaUrl('tours', 'normal')}} 600w, {{$tour->getFirstMediaUrl('tours', 'double')}} 900w, {{$tour->getFirstMediaUrl('tours', 'double')}} 1440w" src="{{$tour->getFirstMediaUrl('tours', 'double')}}" type="{{$tour->getFirstMedia('tours')->mime_type}}" alt="{{$tour->title}}" class="w-100" />
            </picture>
          </div>
          <div class="p-4 text-center">
            <h4 class="tour-title mb-3">{{$tour->title}}</h4>
            <p>{{substr($tour->excerpt,0,120)}} [...]</p>
            <a href="{{route('tours-show', ['slug' => $tour->slug])}}">
              <button class="btn btn-primary" type="button">Find out more</button>
            </a>
          </div>
        </div>
    </div>
    @endforeach
  </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')
@endsection