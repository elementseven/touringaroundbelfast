@php
$page = 'Tour';
$pagetitle = $tour->title . ' | Touring Around Belfast';
$metadescription = $tour->excerpt;
$pagetype = 'dark';
$pagename = 'tour';
$ogimage = $tour->getFirstMediaUrl('tours');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative bg py-5 bg-down-up" style="background-image:url('{{$tour->getFirstMediaUrl('tours')}}');">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container z-2 position-relative py-5 mob-pb-0">
      <div class="row py-5 mob-py-0">
        <div class="col-12 pt-5 mob-py-0">
          <div class="row">
            <div class="col-xl-11 text-white">
              @if($tour->price)
              <p><b>{{$tour->price}}</b></p>
              @endif
              <h1 class="text-white mb-3">{{$tour->title}}</h1>
              <p class="text-white text-large mb-5">{{$tour->excerpt}}</p>
              <p class="mb-1"><b>Share this tour: </b></p>
              <p class="text-larger">
                <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn mr-2 text-white">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($tour->title)}}&amp;summary={{urlencode($tour->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn mr-2 text-white">
                  <i class="fa fa-linkedin"></i>
                </a>
                <a href="https://twitter.com/intent/tweet/?text={{urlencode($tour->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn mr-2 text-white">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="whatsapp://send?text={{urlencode($tour->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn text-white">
                  <i class="fa fa-whatsapp"></i>
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-mb-0 tour-body">
  <div class="row">
    <div class="container py-5 my-5 mob-my-0">
      <div class="row">
        <div class="col-12 text-left mob-px-4">
          <h2 class="mb-4">Tour Details</h2>
        </div>
        <div class="col-lg-8 text-left mob-px-4 mob-mb-5 ipadp-mb-5">
          {!!$tour->description!!}
          <div class="row">
            <div class="col-5 col-lg-4">
              <p class="mb-1 mt-5"><b>Share this tour: </b></p>
              <p class="text-larger">
                <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn mr-2">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($tour->title)}}&amp;summary={{urlencode($tour->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn mr-2">
                  <i class="fa fa-linkedin"></i>
                </a>
                <a href="https://twitter.com/intent/tweet/?text={{urlencode($tour->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn mr-2">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="whatsapp://send?text={{urlencode($tour->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn">
                  <i class="fa fa-whatsapp"></i>
                </a>
              </p>
            </div>
            <div class="col-7 col-lg-4">
              <p class="mb-1 mt-5"><b>See more tours: </b></p>
              <p class="text-larger"><a href="{{route('tours')}}"><b><i class="fa fa-angle-double-left"></i> Back to tours page</b></a></p>
            </div>
          </div>
        </div>
        <follow-box :tour="{{$tour}}" :recaptcha="'{{env('RECAPTCHA_V3_SITE_KEY')}}'"></follow-box>
      </div>
    </div>
  </div>
</div>
@if(count($tour->pictures))
<div class="container pb-5 mob-pt-0">
  <div class="row mb-5 half_row">
    @foreach($tour->pictures as $key => $picture)
    @if($key <=5)
    <div class="col-lg-4 col-6 half_col mb-3">
      <picture> 
        <source srcset="{{$picture->getFirstMediaUrl('pictures','square-webp')}}" type="image/webp"/> 
        <source srcset="{{$picture->getFirstMediaUrl('pictures','square')}}" type="{{$picture->getFirstMedia('pictures')->mime_type}}"/>
        <img src="{{$picture->getFirstMediaUrl('pictures','square')}}" alt="{{$tour->title}} image" class="w-100 featured-image"/>
      </picture>
    </div>
    @endif
    @endforeach
  </div>
</div>
@endif
<div class="container pb-5 mb-5">
  <div class="row justify-content-center">
    <div class="col-12 text-center mb-4 mob-px-4">
      <h2>Request a booking</h2>
      <p><b>Fancy going on our {{$tour->title}}?</b><br>Leave your details below and we will get back to you to confirm your booking.</p>
    </div>
    <div class="col-lg-10 mob-px-4">
      <tour-form :tour="{{$tour}}" btn="primary" labels="dark" dateid="date2" columnsize="col-lg-6" :recaptcha="'{{env('RECAPTCHA_V3_SITE_KEY')}}'"></tour-form>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $('.book-now').click(function(){
    $('html, body').animate({
      scrollTop: $("#book-box").offset().top -100
    }, 500);
  });
</script>
@endsection
@section('modals')

@endsection