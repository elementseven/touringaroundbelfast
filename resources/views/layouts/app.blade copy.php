<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="/img/favicon/site.webmanifest">
	<link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#000000">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<title>{{$pagetitle}}</title>

	<meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Xeroplas">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:description" content="{{$metadescription}}">

	<meta name="description" content="{{$metadescription}}">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
	@yield('styles')
	<script type="application/ld+json">
	{"@context" : "https://schema.org",
	 "@type" : "Organization",  	    "telephone": "+442890123456",
	    "contactType": "Customer service"
	  }
	}
	</script>
	<script>window.dataLayer = window.dataLayer || [];</script>
  <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WMJH7PC');</script>
	<!-- End Google Tag Manager -->
</head>
<body class="front">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WMJH7PC"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	@yield('fbroot')
	<div id="main-wrapper">
		<div id="app" class="front">
			<div class="container-fluid px-5 mob-px-3 bg-primary py-1 mob-py-0">
				<div class="row">
					<div class="col-lg-6"></div>
					<div class="col-lg-6 text-right">
						<p class="mb-0 text-smaller text-title">
							<b><a href="mailto:hello@xeroplas.com" class="text-white mob-float-left">hello@xeroplas.com</a></b>
							<a href="https://www.facebook.com/XeroPlas" class="text-white"><i class="fa fa-facebook ml-4"></i></a>
							<a href="https://www.instagram.com/xeroplasrecycling" class="text-white"><i class="fa fa-instagram ml-3"></i></a>
							<a href="https://twitter.com/xeroplas" class="text-white"><i class="fa fa-twitter ml-3"></i></a>
							<a href="https://www.linkedin.com/in/colin-thompson-aaa96419b/" class="text-white"><i class="fa fa-linkedin ml-3"></i></a>
						</p>
					</div>
				</div>
			</div>
			<div id="menu_btn" class="menu_btn float-left d-lg-none d-none"><div class="nav-icon"><span></span><span></span><span></span></div></div>
			<div class="menu position-relative">
				<div class="container-fluid px-5 mob-px-3">
					<div class="row">
						<div class="col-2 col-lg-2 pt-4 mob-pt-3">
							<a href="/">
								<img src="/img/logos/logo.svg" class="menu_logo" alt="Xeroplas Logo"/>
							</a>
						</div>
						@if($page != '404')
						<div class="col-lg-10 pt-4 mt-2 text-right d-none d-lg-block">
							<div class="menu-links d-block ">
					
								<a href="{{route('recycle')}}" class="menu-item @if($page == 'How it works') active @endif">How it works</a>
								<a href="{{route('about')}}" class="menu-item @if($page == 'About') active @endif">About</a>
								<a href="{{route('news')}}" class="menu-item @if($page == 'News') active @endif">News</a>
								<a href="{{route('faqs')}}" class="menu-item @if($page == 'Faqs') active @endif">Faqs</a>
								<a href="{{route('contact')}}" class="menu-item @if($page == 'Contact') active @endif">Contact</a>
								<a href="{{route('signup')}}" class="menu-item text-primary @if($page == 'Recycle Now') active @endif">Recycle Now</a>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
			<div id="scroll-menu" class="menu">
				<div class="container-fluid px-5 mob-px-3">
					<div class="row">
						<div class="col-2 col-lg-1 py-4 mob-py-3">
							<a href="/">
								<img src="/img/logos/logo.svg" class="menu_logo" alt="Xeroplas Logo"/>
							</a>
						</div>
						<div class="col-10 col-lg-11 text-right d-none d-lg-block">
							<div class="menu-links d-block py-4 mt-1">
								<a href="{{route('recycle')}}" class="menu-item @if($page == 'How it works') active @endif">How it works</a>
								<a href="{{route('about')}}" class="menu-item @if($page == 'About') active @endif">About</a>
								<a href="{{route('news')}}" class="menu-item @if($page == 'News') active @endif">News</a>
								<a href="{{route('faqs')}}" class="menu-item @if($page == 'Faqs') active @endif">Faqs</a>
								<a href="{{route('contact')}}" class="menu-item @if($page == 'Contact') active @endif">Contact</a>
								<a href="{{route('signup')}}" class="menu-item text-primary @if($page == 'Recycle Now') active @endif">Recycle Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="mobile-menu" class="mobile-menu">
				<div class="container-fluid px-3">
					<div class="row">
						<a href="/">
							<img src="/img/logos/logo.svg" class="menu_logo" width="50" alt="Xeroplas Logo"/>
						</a>
						<div class="col-lg-10 pt-5 mt-5">
							<div class="menu-links d-block pt-4">
								<p><a href="{{route('recycle')}}" class="menu-item @if($page == 'How it works') active @endif">How it works</a></p>
								<p><a href="{{route('about')}}" class="menu-item @if($page == 'About') active @endif">About</a></p>
								<p><a href="{{route('news')}}" class="menu-item @if($page == 'News') active @endif">News</a></p>
								<p><a href="{{route('faqs')}}" class="menu-item @if($page == 'Faqs') active @endif">Faqs</a></p>
								<p><a href="{{route('contact')}}" class="menu-item @if($page == 'Contact') active @endif">Contact</a></p>
								<a href="{{route('signup')}}" class="menu-item text-primary @if($page == 'Recycle Now') active @endif">
									<div class="btn btn-primary w-100">Recycle Now</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			@yield('header')
			<main id="content">
				<div id="menu-trigger"></div>
				@yield('content')
			</main>
			@if($page != '404')

			<footer class="pt-5 pb-0 container-fluid text-center text-md-left position-relative z-2">
				<div class="row py-2 mob-mt-0">
					<div class="container">
						<div class="row text-center justify-content-center">
							<div class="col-lg-3 mb-3">
								<img src="/img/logos/logo-white.svg" width="200" alt="Xeroplas Recycling" class="mb-3" />
								<p class="mb-2">
									<a href="https://www.facebook.com/XeroPlas" class="text-white"><i class="fa fa-facebook"></i></a>
									<a href="https://www.instagram.com/xeroplasrecycling/" class="text-white"><i class="fa fa-instagram ml-3"></i></a>
									<a href="https://twitter.com/xeroplas" class="text-white"><i class="fa fa-twitter ml-3"></i></a>
									<a href="https://www.linkedin.com/in/colin-thompson-aaa96419b/" class="text-white"><i class="fa fa-linkedin ml-3"></i></a>
								</p>
								<p class="text-small"><a href="mailto:hello@xeroplas.com">hello@xeroplas.com</a></p>
							</div>

						</div>
					</div>
				</div>
				<div class="row text-center">
					<div class="col-12">
						<p class="text-smaller">&copy;{{Carbon\Carbon::now()->format('Y')}} Xeroplas Ltd | <a href="https://elementseven.co" target="_blank">Website by Element Seven</a></p>
					</div>
				</div>
			</footer>
			@endif
			@yield('modals')
		</div>
		<div id="menu_body_hide"></div>
		<div id="loader">
			<div class="vert-mid">
				<div class="card p-5">
					<img id="loader-success" src="/img/icons/success.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
					<img id="loader-error" src="/img/icons/error.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
					<div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
					<div id="loader-message"></div>
					<div class="container-fluid">
						<div class="row">
							<div class="container">
								<div class="row justify-content-center">
									<div class="col-md-6">
										@yield('loader-buttons')
										<a id="loader-link">
											<div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
										</a>
										<div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
									</div>
									<div class="col-md-12 text-center">
										<p id="loader-second-text" class="mt-3 d-none cursor-pointer"><a id="loader-second-link"></a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="{{ asset('js/app.js') }}"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#1E1E1E",
	      "text": "#ffffff"
	    },
	    "button": {
	      "background": "#38B75C",
	      "text": "#ffffff"
	    }
	  }
	})});
	</script>
	@yield('scripts')
</body>
</html>