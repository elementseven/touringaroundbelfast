<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="/img/favicon/site.webmanifest">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<title>{{$pagetitle}}</title>

	<meta property="og:type" content="website">
	<meta property="og:title" content="{{$pagetitle}}">
	<meta property="og:url" content="{{Request::url()}}">
	<meta property="og:site_name" content="Touring Around Belfast">
	<meta property="og:locale" content="en_GB">
	<meta property="og:image" content="{{$ogimage}}">
	<meta property="og:description" content="{{$metadescription}}">

	<meta name="description" content="{{$metadescription}}">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
	@yield('styles')
	<script type="application/ld+json">
		{"@context" : "https://schema.org",
		"@type" : "Organization",  	    
		"telephone": "+447971895746",
		"contactType": "Customer service"
	}
}
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MTHKRNX');</script>
<!-- End Google Tag Manager -->
</head>
<body class="front {{$pagename}} {{$pagetype}}">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MTHKRNX"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	@yield('fbroot')
	<div id="main-wrapper">
		<div id="app" class="front">
			<div id="menu_btn" class="menu_btn float-left d-lg-none"><div class="nav-icon"><span></span><span></span><span></span></div></div>
			<div id="main-menu" class="menu">
				<div class="container-fluid px-5 mob-px-3 mt-4">
					<div class="row">
						<div class="col-lg-2 col-3">
							<a href="/">
								<picture> 
		              <source srcset="/img/logos/blue-badge-logo.webp" type="image/webp"/> 
		              <source srcset="/img/logos/blue-badge-logo.jpg" type="image/png"/>
		              <img srcset="/img/logos/blue-badge-logo.png" alt="Touring Around Belfast Logo" width="100" class="lazy menu_logo"/>
		            </picture>
							</a>
						</div>
						@if($page != '404')
						<div class="col-lg-10 col-9 text-right d-none d-lg-block pt-2">
							<div class="menu-links d-inline-block">
								<a href="{{route('welcome')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Home</a>
							</div>
							<div class="menu-links d-inline-block">
								<a href="{{route('tours')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Tours</a>
							</div>
							<div class="menu-links d-inline-block ">
								<a href="{{route('about')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">About</a>
							</div>
							<div class="menu-links d-inline-block ">
								<a href="{{route('testimonials')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Testimonials</a>
							</div>
							<div class="menu-links d-inline-block">
								<a href="{{route('contact')}}" class="menu-item @if($pagetype == 'light') text-dark @else text-white @endif cursor-pointer">Contact</a>
							</div>
							<div class="menu-links d-inline-block">		
								<a href="https://www.facebook.com/touringaroundbelfast/" class=" @if($pagetype == 'light') text-dark @else text-white @endif " target="_blank"><i class="fa fa-facebook ml-4"></i></a>
								<a href="https://twitter.com/TouringBelfast" class=" @if($pagetype == 'light') text-dark @else text-white @endif " target="_blank"><i class="fa fa-twitter ml-3"></i></a>
								<a href="mailto:billy@touringaroundbelfast.com" class=" @if($pagetype == 'light') text-dark @else text-white @endif " target="_blank"><i class="fa fa-envelope ml-3"></i></a>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
			<div id="scroll-menu" class="menu d-none d-lg-block">
				<div class="container-fluid px-5 mob-px-3  my-4">
					<div class="row">
						<div class="col-4 col-lg-2 mob-py-3">
							<a href="/">
								<picture> 
		              <source srcset="/img/logos/blue-badge-logo.webp" type="image/webp"/> 
		              <source srcset="/img/logos/blue-badge-logo.jpg" type="image/png"/>
		              <img srcset="/img/logos/blue-badge-logo.png" alt="Touring Around Belfast Logo" width="100" class="lazy menu_logo"/>
		            </picture>
							</a>
						</div>
						<div class="col-8 col-lg-10 text-right d-none d-lg-block pt-2">
							<div class="menu-links d-inline-block">
								<a href="{{route('welcome')}}" class="menu-item text-dark cursor-pointer">Home</a>
							</div>
							<div class="menu-links d-inline-block">
								<a href="{{route('tours')}}" class="menu-item text-dark cursor-pointer">Tours</a>
							</div>
							<div class="menu-links d-inline-block ">
								<a href="{{route('about')}}" class="menu-item text-dark cursor-pointer">About</a>
							</div>
							<div class="menu-links d-inline-block ">
								<a href="{{route('testimonials')}}" class="menu-item text-dark cursor-pointer">Testimonials</a>
							</div>
							<div class="menu-links d-inline-block">
								<a href="{{route('contact')}}" class="menu-item text-dark cursor-pointer">Contact</a>
							</div>
							<div class="menu-links d-inline-block">		
								<a href="https://www.facebook.com/touringaroundbelfast/" class=" text-dark" target="_blank"><i class="fa fa-facebook ml-4"></i></a>
								<a href="https://twitter.com/TouringBelfast" class="text-dark" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
								<a href="mailto:billy@touringaroundbelfast.com" class="text-dark" target="_blank"><i class="fa fa-envelope ml-3"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="mobile-menu" class="mobile-menu">
				<div class="container-fluid px-3">
					<div class="row">
						<a href="/">
							<picture> 
	              <source srcset="/img/logos/logo.webp" type="image/webp"/> 
	              <source srcset="/img/logos/logo.png" type="image/png"/>
	              <img srcset="/img/logos/logo.png" alt="Touring Around Belfast Logo" width="150" class="lazy menu_logo"/>
	            </picture>
						</a>
						<div class="col-lg-10 pt-5 mt-5">
							<div class="menu-links d-block pt-4">
								<p class="mb-0"><a href="{{route('welcome')}}" class="menu-item">Home</a></p>
								<p class="mb-0"><a href="{{route('tours')}}" class="menu-item">Tours</a></p>
								<p class="mb-0"><a href="{{route('about')}}" class="menu-item mb-0">About</a></p>
								<p class="mb-0"><a href="{{route('testimonials')}}" class="menu-item mb-0">Testimonials</a></p>
								<p class="mb-0"><a href="{{route('contact')}}" class="menu-item">Contact</a></p>
								<hr class="mb-4">
								<p class="mb-0">
									<b><a href="mailto:billy@touringaroundbelfast.com" class="text-dark mob-float-left pb-2" target="_blank">billy@touringaroundbelfast.com</a></b><br><br>
									<a href="https://www.facebook.com/touringaroundbelfast/" class="text-primary" target="_blank"><i class="fa fa-facebook "></i></a>
									<a href="https://twitter.com/TouringBelfast" class="text-primary" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			@yield('header')
			<main id="content">
				<div id="menu-trigger"></div>
				@yield('content')
			</main>
			@if($page != '404')

			<footer class="pt-5 pb-0 container-fluid text-center text-md-left position-relative">
				<div class="row py-5 mob-mt-0">
					<div class="container">
						<div class="row text-center justify-content-center">
							<div class="col-lg-4 mb-3">
								<picture> 
		              <source srcset="/img/logos/logo.webp" type="image/webp"/> 
		              <source srcset="/img/logos/logo.png" type="image/png"/>
		              <img srcset="/img/logos/logo.png" alt="Touring Around Belfast" width="250" class="lazy mb-3"/>
		            </picture>
								<p class="mb-2 mt-2"><i class="fa fa-phone mr-2"></i> <b><a href="tel:00447798602401">+44 (0) 77 9860 2401</a></b></p>
								<p class=""><a href="mailto:billy@touringaroundbelfast.com"><b>billy@touringaroundbelfast.com</b></a></p>
								<p class="mb-2 text-larger">
									<a href="https://www.facebook.com/touringaroundbelfast/" target="_blank" class="text-white"><i class="fa fa-facebook"></i></a>
									<a href="https://twitter.com/TouringBelfast" target="_blank" class="text-white"><i class="fa fa-twitter ml-3"></i></a>
								</p>
								{{-- <p class="text-small"><a href="{{route('privacy-policy')}}">Privacy Policy</a> | <a href="{{route('tandcs')}}">Terms & Conditions</a></p> --}}
							</div>
							<div class="col-lg-4">
								<p class="text-large mb-1"><b><a href="{{route('tours')}}" class="menu-item">Tours</a></b></p>
								<p class="text-large mb-1"><b><a href="{{route('about')}}" class="menu-item mb-0">About</a></b></p>
								<p class="text-large mb-1"><b><a href="{{route('testimonials')}}" class="menu-item mb-0">Testimonials</a></b></p>
								<p class="text-large mb-1"><b><a href="{{route('contact')}}" class="menu-item">Contact</a></b></p>
							</div>
							<div class="col-lg-3 col-8 offset-1 mob-mt-5">
								<div class="row text-left">
									<opening-hours></opening-hours>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="row text-center">
					<div class="col-12 mb-3">
						<p class="text-smaller">&copy;{{Carbon\Carbon::now()->format('Y')}} Touring Around Belfast <span class="d-none d-md-inline">|</span><br class="d-md-none"/><a href="https://elementseven.co" target="_blank"> Website by Element Seven</a></p>
					</div>
				</div>
			</footer>
			@endif
			@yield('modals')
		</div>
		<div id="menu_body_hide"></div>
		<div id="loader">
			<div class="vert-mid">
				<div class="card p-5">
					<img id="loader-success" srcset="/img/icons/success.svg" class="lazy d-none mx-auto" width="80" alt="Success icon"/>
					<img id="loader-error" srcset="/img/icons/error.svg" class="lazy d-none mx-auto" width="80" alt="Success icon"/>
					<div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
					<div id="loader-message"></div>
					<div class="container-fluid">
						<div class="row">
							<div class="container">
								<div class="row justify-content-center">
									<div class="col-md-12 text-center">
										<p id="loader-second-text" class="mt-3 d-none cursor-pointer"><a id="loader-second-link"></a></p>
									</div>
									<div class="col-md-6">
										@yield('loader-buttons')
										<a id="loader-link">
											<div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
										</a>
										<div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@yield('prescripts')
	<script src="{{ asset('js/app.js') }}"></script>
	<script>
	window.addEventListener("load", function(){
		window.cookieconsent.initialise({
			"palette": {
				"popup": {
					"background": "#248D5B",
					"text": "#ffffff"
				},
				"button": {
					"background": "#fff",
					"text": "#248D5B"
				}
			}
		})});
	</script>
	@yield('scripts')
</body>
</html>
{!!  GoogleReCaptchaV3::init() !!}