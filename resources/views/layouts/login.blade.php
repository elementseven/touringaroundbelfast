<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="">
  <link rel="canonical" href="https://tommorrison.uk/">
  <meta name="description" content="">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <meta property="og:type" content="website">
  <meta property="og:title" content="Tom Morrison">
  <meta property="og:url" content="https://tommorrison.uk/">
  <meta property="og:site_name" content="Tom Morrison">
  <meta property="og:locale" content="en_GB">

  <title>Causeway Aero - login</title>
  <!-- Styles -->
  <link href="{{ asset('css/typekit.css') }}" rel="stylesheet" async defer/>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
</head>

<body class="front">
  <div id="main-wrapper">
    <div id="app" class="front">
      <desktop-menu :pagetype="'{{$pagetype}}'"></desktop-menu>
      <mobile-menu></mobile-menu>
      @yield('header')
      <main id="content">
        <div id="menu-trigger"></div>
        @yield('content')
      </main>
      <site-footer></site-footer>
      @yield('modals')
    </div>
    <div id="menu_body_hide"></div>
    <loader></loader>
    <div id="loader">
      <div class="vert-mid">
        <img id="loader-success" src="/img/icons/success.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
        <div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <div id="loader-message"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-md-4">
                  @yield('loader-buttons')
                  <a id="loader-link">
                    <div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
                  </a>
                  <div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
                </div>
                <div class="col-md-12 text-center">
                  <p id="loader-second-text" class="mt-3 d-none cursor-pointer"><a id="loader-second-link"></a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="orientation_change">
    <p class="text-white">This website is best experienced in portrait mode, please rotate your device.</p>
    <picture> 
          <source srcset="/img/icons/mobile-rotate.webp" type="image/webp"> 
          <source srcset="/img/icons/mobile-rotate.png" type="image/jpg">
          <img src="/img/icons/mobile-rotate.png" alt="Please rotate your device" class="img-fluid"/>
        </picture>
  </div>
  <script src="{{ asset('js/app.js') }}"></script>
  <script>
  window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#E0261C",
        "text": "#ffffff"
      },
      "button": {
        "background": "#fff",
        "text": "#E0261C"
      }
    }
  })});
  </script>
  @yield('scripts')
</body>
</html>