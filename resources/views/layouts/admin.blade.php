<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#c4d600">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#414141">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <title>Admin System | Belfast Hidden Tours</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link href="{{ asset('admin/css/admin.css') }}" rel="stylesheet">
    <style>@import url("https://use.typekit.net/otr3wfp.css");</style>
    @yield('styles')
    
</head>
<body class="front">
    <div id="main-wrapper">
        <div id="app" class="admin">
            <nav class="navbar fixed-top navbar-expand-lg bg-second d-block px-4 py-3 text-left">
                <a href="{{route('admin')}}">
                    <img src="/img/logos/logo-white.svg" width="70" class="ml-2 mob-mx-0" alt="Belfast Hidden Tours logo"/>
                </a>
                <div id="menu_btn" class="menu_btn float-left"><div class="nav-icon"><span></span><span></span><span></span></div></div>
            </nav>
            <nav class="navbar navbar-expand-lg bg-primary d-block py-2 text-center">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <p class="mb-0 bcrumb"><b>{!!$bcrumb!!}</b></p>
                        </div>
                    </div>
                </div>
            </nav>
            <div id="menu">
                <div class="menu_inner container-fluid px-4">
                    <div class="row">
                        <div class="col-12">
                            <img src="/img/logos/logo-dark.svg" width="100" class="mb-5" alt="Belfast Hidden Tours logo"/>
                        </div>
                        <div class="col-12 menu_item pb-3">
                            <a href="{{route('admin')}}">
                                <div class="row">
                                    <div class="col-10"><p class="mb-0"><b>Dashboard</b></p></div>
                                    <div class="col-2 text-right"><p class="mb-0"><i class="fa fa-angle-right"></i></p></div>
                                    <div class="col-12"><hr class="mt-3 mb-0"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 menu_item pb-3">
                            <a href="{{route('admin.tours')}}">
                                <div class="row">
                                    <div class="col-10"><p class="mb-0"><b>Tours</b></p></div>
                                    <div class="col-2 text-right"><p class="mb-0"><i class="fa fa-angle-right"></i></p></div>
                                    <div class="col-12"><hr class="mt-3 mb-0"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 menu_item pb-3">
                            <a href="{{route('admin.news')}}">
                                <div class="row">
                                    <div class="col-10"><p class="mb-0"><b>News</b></p></div>
                                    <div class="col-2 text-right"><p class="mb-0"><i class="fa fa-angle-right"></i></p></div>
                                    <div class="col-12"><hr class="mt-3 mb-0"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 mt-4">
                            <a href="/logout">
                                <div class="btn btn-primary shadow">Sign Out</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content">
              @yield('content')
            </div>
            @yield('modals')
        </div>
        <div id="menu_body_hide"></div>
        <div id="loader">
            <div class="vert-mid">
                <img id="loader-success" src="/img/icons/tick.svg" class="d-none mx-auto" width="100" alt="Success icon"/>
                <div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                <div id="loader-message"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-md-4">
                                    @yield('loader-buttons')
                                    <a id="loader-link">
                                        <div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
                                    </a>
                                    <div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <p id="loader-second-text" class="mt-3 d-none cursor-pointer"><a id="loader-second-link"></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('admin/js/admin.js') }}?v0.13"></script>
    @yield('scripts')
</body>
</html>