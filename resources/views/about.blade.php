@php
$page = 'About';
$pagetitle = 'About | Touring Around Belfast';
$metadescription = 'About';
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://touringaroundbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')

<header class="container-fluid position-relative bg py-5 about-top bg-down-up">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container z-2 position-relative py-5">
      <div class="row py-5 mob-py-0">
        <div class="col-12 pt-5 mob-py-0">
          <div class="row">
            <div class="col-xl-9 text-white mob-px-4">
              <h1 class="text-white mb-3">About</h1>
              <p class="text-white text-large mb-5"><b>Please review our current tours and if there is anything you would like to see or learn about then let us know and we be happy to arrange an itinerary specially designed for you.</b></p>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container my-5 py-5">
	<div class="row">
    <div class="col-lg-6 mob-px-4 mob-mb-5">
      <h2>About Belfast & Northern Ireland</h2>
      <p>Belfast is the capital of N.Ireland geographically part of the island of Ireland, but politically in the UK, so from the start of your visit you will find yourself in a land of contrasts – geographically, politically and culturally, from the marble Arch Caves to the basalt columns at the Causeway, fiddles banjos and Lambeg drums all brought together in a great big eclectic bundle of pick and mix.</p>
      <p>Fairy glens to ceilli bars put on your mountain boots or high heeled shoes, tap your toes to traditional musicians or punk rock bands, swing across Carrick-a-Rede rope bridge or jig a reel across the dance floor. </p>
      <p>Listen to mythical tales and visit world heritage sites in a land that provided America with a quarter of its Presidents, built the Titanic and invented air conditioning – there’s just so much more to see and do in Belfast and beyond.</p>
      <a href="{{route('tours')}}">
        <button class="btn btn-primary">Browse Tours</button>
      </a>
    </div>
    <div class="col-lg-6 pl-5 mob-px-4 ipadp-mt-5 ipadp-px-3">
      <picture> 
        <source srcset="/img/about/about_2.webp" type="image/webp"/> 
        <source srcset="/img/about/about_2.jpg" type="image/jpg"/>
        <img srcset="/img/about/about_2.jpg" alt="About us - Touring Around Belfast" class="w-100 lazy shadow"/>
      </picture>
    </div>
  </div>
</div>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-10">
      <div class="row justify-content-center">
        <div class="col-lg-3 col-6 text-center py-3">
          <a href="https://www.itg.org.uk/" target="_blank">
            <picture> 
              <source srcset="/img/logos/tourism/tourist-guide.webp" type="image/webp"/> 
              <source srcset="/img/logos/tourism/tourist-guide.jpg" type="image/jpg"/>
              <img srcset="/img/logos/tourism/tourist-guide.jpg" alt="Insitute of tourist guiding" class="w-100 lazy"/>
            </picture>
          </a>
        </div>
        <div class="col-lg-3 col-6 text-center py-3">
          <a href="https://www.embraceagiantspirit.com/" target="_blank">
            <picture> 
              <source srcset="/img/logos/tourism/giant_spirit.webp?v2.0" type="image/webp"/> 
              <source srcset="/img/logos/tourism/giant_spirit.jpg?v2.0" type="image/jpg"/>
              <img srcset="/img/logos/tourism/giant_spirit.jpg?v2.0" alt="Embrace a giant spirit logo" class="w-100 lazy"/>
            </picture>
          </a>
        </div>
        <div class="col-lg-3 col-6 text-center py-3">
          <a href="https://covid19.tourismni.com/support-centre/business-support-advice/were-good-to-go/" target="_blank">
            <picture> 
              <source srcset="/img/logos/tourism/good_to_go.webp" type="image/webp"/> 
              <source srcset="/img/logos/tourism/good_to_go.jpg" type="image/jpg"/>
              <img srcset="/img/logos/tourism/good_to_go.jpg" alt="We're good to go logo" class="w-100 lazy"/>
            </picture>
          </a>
        </div>
        
      </div>
    </div>
  </div>
</div>
<div class="container pb-5 mb-5">
  <div class="row mt-5 pt-5 mob-mt-0">
    <div class="col-lg-6 pr-5 mob-px-4 d-none d-lg-block">
      <picture> 
        <source srcset="/img/about/about_1.webp" type="image/webp"/> 
        <source srcset="/img/about/about_1.jpg" type="image/jpg"/>
        <img srcset="/img/about/about_1.jpg" alt="About us 2 - Touring Around Belfast" class="w-100 lazy shadow"/>
      </picture>
    </div>
    <div class="col-lg-6 mob-px-4 mob-mb-5 ipadp-mb-5">
      <p>Billy works regularly for Northern Ireland tourist board, Tourism Ireland and works for many major tour operators such as CIE, EX Ire. McKinley & Kidd. and guides for college groups such as People to People. Touring Around Belfast, Billy Scott is a Bluebadge Guide with a vast experience of dealing with tourism requirements of all types. The Bluebadge is the highest standard of achievement both nationally and internationally, ensuring that each guide is trained and examined to the highest level of knowledge and guiding skills.</p>
      <p>Touring Around Belfast will satisfy all your business and tourism needs specialising in;</p>
      <ul>
        <li><p class="mb-0">Walking Tours</p></li>
        <li><p class="mb-0">Coach Tours</p></li>
        <li><p class="mb-0">Blackcab Tours</p></li>
        <li><p class="mb-0">Exclusive travel (including up-to 8 seat transport)</p></li>
      </ul>
      <a href="{{route('contact')}}">
        <button class="btn btn-primary">Get in touch</button>
      </a>
    </div>
    <div class="col-lg-6 mob-px-4 d-lg-none">
      <picture> 
        <source srcset="/img/about/about_1.webp" type="image/webp"/> 
        <source srcset="/img/about/about_1.jpg" type="image/jpg"/>
        <img srcset="/img/about/about_1.jpg" alt="About us 2 - Touring Around Belfast" class="w-100 lazy shadow"/>
      </picture>
    </div>
  </div>
</div>
<div class="container container-wide px-5 mob-px-3 py-5 mob-pt-0">
  <div class="row">
    <div class="col-12 text-center mb-4">
      <h2 class="mb-0 grey-text">Interested?</h2>
      <p class="below-title">Book a tour now!</p>
    </div>
    @foreach($tours as $tour)
    <div class="col-lg-4 mb-5 mob-px-4">
      
        <div class="card border-0 shadow overflow-hidden tour-box text-center text-md-left text-dark">
          <div class="tour-image">
            <picture> 
              <source media="(min-width: 900px)" srcset="{{$tour->getFirstMediaUrl('tours', 'normal')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <source media="(min-width: 601px)" srcset="{{$tour->getFirstMediaUrl('tours', 'normal-webp')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <source srcset="{{$tour->getFirstMediaUrl('tours', 'normal-webp')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <img srcset="{{$tour->getFirstMediaUrl('tours', 'normal')}} 600w, {{$tour->getFirstMediaUrl('tours', 'double')}} 900w, {{$tour->getFirstMediaUrl('tours', 'double')}} 1440w" src="{{$tour->getFirstMediaUrl('tours', 'double')}}" type="{{$tour->getFirstMedia('tours')->mime_type}}" alt="{{$tour->title}}" class="w-100" />
            </picture>
          </div>
          <div class="p-4 text-center">
            <h4 class="tour-title mb-3">{{$tour->title}}</h4>
            <p>{{substr($tour->excerpt,0,120)}} [...]</p>
            <a href="{{route('tours-show', ['slug' => $tour->slug])}}">
              <button class="btn btn-primary" type="button">Find out more</button>
            </a>
          </div>
        </div>
    </div>
    @endforeach
  </div>
</div>
@endsection