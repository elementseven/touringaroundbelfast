@php
$page = '404';
$pagetitle = '404 - Touring Around Belfast';
$metadescription = 'We couldnt find the page you were looking for.';
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://touringaroundbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative">
    <div class="row py-5">
        <div class="col-lg-12 text-center">
	        <img alt="service" width="300" class="lazy mb-5" data-srcset="/img/home/installation.svg"/>
            <h1 class="page-top text-primary scroll-fade mw-100" data-fade="fadeIn">&nbsp;404&nbsp;</h1>
            <p class="statement mb-4">Sorry, we couldn't find the page you were looking for</p>
            <a href="{{route('welcome')}}">
                <div class="btn btn-primary btn-big mb-5">Home Page</div>
            </a>
        </div>
    </div>
</header>
@endsection
@section('content')

@endsection
@section('scripts')

@endsection
@section('modals')
@endsection