@php
$page = 'Contact';
$pagetitle = 'Contact | Touring Around Belfast';
$metadescription = 'Get in touch with Touring Around Belfast. We are happy to answer any and all questions you may have!';
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://touringaroundbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'og_image' => $ogimage])
@section('header')
<header class="container-fluid position-relative bg py-5 contact-top bg-down-up">
  <div class="trans"></div>
  <div class="row py-5">
    <div class="container z-2 position-relative py-5">
      <div class="row py-5 mob-py-0">
        <div class="col-12 pt-5 mob-py-0">
          <div class="row">
            <div class="col-xl-9 text-white mob-px-4">
              <h1 class="text-white mb-0">Get in touch</h1>
              <p class="text-white text-large mb-5"><b>We would love to hear from you!</b></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container py-5 ">
    <div class="row my-5 mob-mb-0">
        <div class="col-lg-8 position-relative z-2 pr-5 mob-px-3 mt-minus-small">
            <div class="card border-0 shadow py-5 px-5 pb-3 mob-px-4">
                <h3 class="mb-3">Contact us</h3>
                <contact-page-form :recaptcha="'{{env('RECAPTCHA_V3_SITE_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
            </div>
        </div>
        <div class="col-lg-4 text-center text-lg-left mob-mt-5 ipadp-mt-5">
            <h3 class="mb-3">Contact Details</h3>
            <p><i class="fa fa-phone mr-2"></i> <b><a href="tel:00447798602401">+44 (0) 77 9860 2401</a></b></p>
            <p><i class="fa fa-envelope mr-2"></i> <b><a href="mailto:billy@touringaroundbelfast.com">billy@touringaroundbelfast.com</a></b></p>
            <p class="mb-0 text-larger text-title">
                <a href="https://www.facebook.com/touringaroundbelfast/" class="" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/TouringBelfast" class="" target="_blank"><i class="fa fa-twitter ml-3"></i></a>
            </p>        
        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')
@endsection