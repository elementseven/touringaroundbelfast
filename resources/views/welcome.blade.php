@php
$page = 'Homepage';
$pagetitle = 'Touring Around Belfast | Sightseeing, Political & Walking tours in Northern Ireland';
$metadescription = "We provide a great range of tours covering Belfast’s troubles & political past, tours of world famous murals and tours around Northern Ireland's most scenic locations, inlcuding Game of Thrones.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://touringaroundbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative full-height home-top bg bg-down-up"></header>
@endsection
@section('content')
<div class="container container-wide mt-minus position-relative z-2">
  <div class="row">
    <div class="col-12 mob-px-4">
      <div class="row">
        <div class="col-xl-9">
          <h1 class="text-white mb-0">Touring Around Belfast</h1>
          <p class="below-title text-white">…and beyond.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    @foreach($tours as $tour)
    <div class="col-lg-4 mb-5  mob-px-4">
      <div class="card border-0 shadow overflow-hidden tour-box text-center text-md-left text-dark">
        <div class="tour-image">
          <picture> 
            <source media="(min-width: 900px)" srcset="{{$tour->getFirstMediaUrl('tours', 'normal')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
            <source media="(min-width: 601px)" srcset="{{$tour->getFirstMediaUrl('tours', 'normal-webp')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
            <source srcset="{{$tour->getFirstMediaUrl('tours', 'normal-webp')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
            <img srcset="{{$tour->getFirstMediaUrl('tours', 'normal')}} 600w, {{$tour->getFirstMediaUrl('tours', 'double')}} 900w, {{$tour->getFirstMediaUrl('tours', 'double')}} 1440w" src="{{$tour->getFirstMediaUrl('tours', 'double')}}" type="{{$tour->getFirstMedia('tours')->mime_type}}" alt="{{$tour->title}}" class="w-100" />
          </picture>
        </div>
        <div class="p-4 text-center">
          <h4 class="tour-title mb-3">{{$tour->title}}</h4>
          <p>{{substr($tour->excerpt,0,120)}} [...]</p>
          <a href="{{route('tours-show', ['slug' => $tour->slug])}}">
            <button class="btn btn-primary" type="button">Find out more</button>
          </a>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="container container-wide py-5 my-5 mob-my-0 mob-pt-0 ipadp-mt-0">
  <div class="row">
    <div class="col-lg-6 pr-5 mob-px-4 mob-mb-5 ipadp-mb-5">
      <p class="text-larger"><b>Billy Scott</b> has been a Blue Badge tourist guide since 2006, a member of The Institute of Tourist Guiding and Northern Ireland Tourist Guides Association.</p>
      <p class="text-larger">The Blue Badge is the UK’s highest award for Tourist Guiding.Billy regularly works as a guide for Tourism Ireland, Tourism N.Ireland and companies such as Excursions Ireland.</p>
      <p class="text-larger">As a Blue Badge Guide he is qualified, licensed and insured to work as a coach guide, walking guide, black cab guide and driver guide to work far and wide along the highways and byways of Northern Ireland and  beyond.
      </p>
      <p class="text-larger">Vehicles are available with wheelchair accessiblity.</p>
    </div>
    <div class="col-lg-6  mob-px-4">
      <picture> 
        <source srcset="/img/home/billy.webp" type="image/webp"/> 
        <source srcset="/img/home/billy.jpg" type="image/jpg"/>
        <img srcset="/img/home/billy.jpg" alt="Billy Scott - Touring Around Belfast" class="w-100 lazy shadow"/>
      </picture>
    </div>
  </div>
  <div class="row justify-content-center py-5 ipad-pb-0 mob-pb-3">
    <div class="col-lg-10 py-5 mob-pb-0">
      <div class="testimonial text-center">
        <img srcset="/img/icons/quote-top.svg" alt="Touring Around Belfast - Quote mark top" class="lazy quote-top"/>
        <p class="testimonial-text">Larger than life Billy Scott is a tour guide veteran, who breaks into song just as quickly as he rattles off his patter about Belfast on his bus!</p>
        <p class="testimonial-title"><b>Ivan Little Belfast Telegraph - 13th April 2020</b></p>
        <img srcset="/img/icons/quote-bottom.svg" alt="Touring Around Belfast - Quote mark bottom" class="lazy quote-bottom"/>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid container-wide position-relative">
  <div class="row">
    <div class="col-12 d-none d-md-block">
      <picture> 
        <source srcset="/img/home/collage.webp" type="image/webp"/> 
        <source srcset="/img/home/collage.jpg" type="image/jpg"/>
        <img srcset="/img/home/collage.jpg" alt="Tourism spots Belfast - Giants Causeway and other tourist locations in Northern Ireland" class="w-100 lazy"/>
      </picture>
    </div>
    <div class="col-12 d-md-none px-0">
      <picture> 
        <source srcset="/img/home/collage-mob.webp" type="image/webp"/> 
        <source srcset="/img/home/collage-mob.jpg" type="image/jpg"/>
        <img srcset="/img/home/collage-mob.jpg" alt="Tourism spots Belfast - Giants Causeway and other tourist locations in Northern Ireland" class="w-100 lazy"/>
      </picture>
    </div>
    <div class="card collage-card border-0 p-5 mob-px-4">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 text-center">
            <h2 class="mb-4">Giants Causeway & Antrim Coast</h2>
            <p class="text-larger">A full day tour along the beautiful Antrim coast regularly voted one of the world’s favourite scenic drives.</p>
            <p class="text-larger">So get your comfy shoes on and bring your waterproofs, just in case, as it has been known to softly drizzle here once in a while!</p>
            <p class="text-larger mb-5">Are you ready to let your adventurous spirit run wild, where the narrative is one of myth and fairies a land of fantasy?</p>
            <a href="{{route('tours-show', ['slug' => 'antrim-coast-giants-causeway'])}}">
              <div class="btn btn-primary">Book Tour</div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row justify-content-center py-5">
    <div class="col-lg-10 py-5 mob-pb-3">
      <div class="testimonial text-center">
        <img srcset="/img/icons/quote-top.svg" alt="Touring Around Belfast - Quote mark top" class="lazy quote-top"/>
        <p class="testimonial-text">As a guest of Tourism Ireland. I travelled with Billy Scott, an exceptional guide who tirelessly explained the country’s centuries-old complexities during a six day visit.</p>
        <p class="testimonial-title"><b>Patricia Schultz (Global Ambassador Trafalgar Travel) - 20th September 2017</b></p>
        <img srcset="/img/icons/quote-bottom.svg" alt="Touring Around Belfast - Quote mark bottom" class="lazy quote-bottom"/>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid container-wide ipadp-mb-5">
  <div class="row position-relative">
    <div class="col-12 d-none d-md-block">
      <picture> 
        <source srcset="/img/home/collage2.webp" type="image/webp"/> 
        <source srcset="/img/home/collage2.jpg" type="image/jpg"/>
        <img srcset="/img/home/collage2.jpg" alt="Tourism in Belfast black cab taxi tours in Northern Ireland" class="w-100 lazy"/>
      </picture>
    </div>
    <div class="col-12 d-md-none px-0">
      <picture> 
        <source srcset="/img/home/collage2-mob.webp" type="image/webp"/> 
        <source srcset="/img/home/collage2-mob.jpg" type="image/jpg"/>
        <img srcset="/img/home/collage2-mob.jpg" alt="Tourism in Belfast black cab taxi tours in Northern Ireland" class="w-100 lazy"/>
      </picture>
    </div>
    <div class="card collage-card collage-card-2 border-0 p-5 mob-px-4">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-12 text-center">
            <h2 class="mb-4">Belfast Black Cab Tours</h2>
            <p class="text-larger">The Black Cab Tour brings you into the communities you’ve seen on television.</p>
            <p class="text-larger">We use the murals when relating to events from an historical perspective and you follow in the footsteps of people such as the Dalai Lama and Bill Clinton and write your message on the Peace wall.</p>
            <p class="text-larger mb-5">These tour’s cover the origins of conflict, the mechanism of conflict and the resolution of conflict.</p>            
            <a href="{{route('tours-show', ['slug' => 'belfast-black-cab-tour'])}}">
              <div class="btn btn-primary">Book Tour</div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5">
  <div class="row justify-content-center">
    <div class="col-lg-10 py-5">
      <div class="row justify-content-center">
        <div class="col-lg-3 col-6 text-center py-3">
          <a href="https://www.itg.org.uk/" target="_blank">
            <picture> 
              <source srcset="/img/logos/tourism/tourist-guide.webp" type="image/webp"/> 
              <source srcset="/img/logos/tourism/tourist-guide.jpg" type="image/jpg"/>
              <img srcset="/img/logos/tourism/tourist-guide.jpg" alt="Insitute of tourist guiding" class="w-100 lazy"/>
            </picture>
          </a>
        </div>
        <div class="col-lg-3 col-6 text-center py-3">
          <a href="https://www.embraceagiantspirit.com/" target="_blank">
            <picture> 
              <source srcset="/img/logos/tourism/giant_spirit.webp?v2.0" type="image/webp"/> 
              <source srcset="/img/logos/tourism/giant_spirit.jpg?v2.0" type="image/jpg"/>
              <img srcset="/img/logos/tourism/giant_spirit.jpg?v2.0" alt="Embrace a giant spirit logo" class="w-100 lazy"/>
            </picture>
          </a>
        </div>
        <div class="col-lg-3 col-6 text-center py-3">
          <a href="https://covid19.tourismni.com/support-centre/business-support-advice/were-good-to-go/" target="_blank">
            <picture> 
              <source srcset="/img/logos/tourism/good_to_go.webp" type="image/webp"/> 
              <source srcset="/img/logos/tourism/good_to_go.jpg" type="image/jpg"/>
              <img srcset="/img/logos/tourism/good_to_go.jpg" alt="We're good to go logo" class="w-100 lazy"/>
            </picture>
          </a>
        </div>
        
      </div>
    </div>
  </div>
</div>
<div class="container-fluid mb-5">
  <div class="row">
    <div class="col-lg-7 bg-primary px-0 py-5 mob-px-3 create-private-tour">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
          <div class="row mob-py-5 pr-5 mob-px-4 text-center text-lg-left justify-content-center">
            <div class="col-xl-11 offset-xl-1 text-white px-5 mob-px-3 ipadp-py-5 ipadp-px-3">
              <h2>Create Your Own Private Tour</h2>
              <p class="text-larger">Northern Ireland is much more than the Giants Causeway and Belfast. </p>
              <p class="text-larger">Danny Boy the Londonderry Air or where the Mountains of Mourne sweep down to the sea in County Down are just a few of many other locations we provide.</p>
              <p class="text-larger">See what else we offer with this tour, making your experience custom to what you want!</p>
              <a href="{{route('tours-show', 'private-tours')}}">
                <div class="btn btn-white mt-3">Learn More</div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-5 px-0">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
          <picture> 
            <source srcset="/img/home/collage3.webp" type="image/webp"/> 
            <source srcset="/img/home/collage3.jpg" type="image/jpg"/>
            <img srcset="/img/home/collage3.jpg" alt="Tourism spots Belfast & Northern Ireland - Giants Causeway, Stormont and others" class="w-100 lazy"/>
          </picture>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container container-wide px-5 mob-px-3 py-5">
  <div class="row">
    <div class="col-12 text-center mb-4">
      <h2 class="mb-0 grey-text">Interested?</h2>
      <p class="below-title">Book a tour now!</p>
    </div>
    @foreach($tours as $tour)
    <div class="col-lg-4 mb-5">
      
        <div class="card border-0 shadow overflow-hidden tour-box text-center text-md-left text-dark">
          <div class="tour-image">
            <picture> 
              <source media="(min-width: 900px)" srcset="{{$tour->getFirstMediaUrl('tours', 'normal')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <source media="(min-width: 601px)" srcset="{{$tour->getFirstMediaUrl('tours', 'normal-webp')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <source srcset="{{$tour->getFirstMediaUrl('tours', 'normal-webp')}} 1x, {{$tour->getFirstMediaUrl('tours', 'double-webp')}} 2x" type="image/webp"/> 
              <img srcset="{{$tour->getFirstMediaUrl('tours', 'normal')}} 600w, {{$tour->getFirstMediaUrl('tours', 'double')}} 900w, {{$tour->getFirstMediaUrl('tours', 'double')}} 1440w" src="{{$tour->getFirstMediaUrl('tours', 'double')}}" type="{{$tour->getFirstMedia('tours')->mime_type}}" alt="{{$tour->title}}" class="w-100" />
            </picture>
          </div>
          <div class="p-4 text-center">
            <h4 class="tour-title mb-3">{{$tour->title}}</h4>
            <p>{{substr($tour->excerpt,0,120)}} [...]</p>
            <a href="{{route('tours-show', ['slug' => $tour->slug])}}">
              <button class="btn btn-primary" type="button">Find out more</button>
            </a>
          </div>
        </div>
    </div>
    @endforeach
  </div>
</div>
@endsection
@section('scripts')

@endsection
@section('modals')

@endsection