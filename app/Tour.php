<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Tour extends Model implements HasMedia
{

	use HasMediaTrait;

	protected $fillable = [
    'title','price','slug','photo','starts_at','excerpt','description','expect','status','category_id'
  ];

  protected static function boot()
    {
        parent::boot();
        static::saving(function ($tour) {
            $tour->slug = Str::slug($tour->title, "-");
        });
    }

  public function registerMediaConversions(Media $media = null)
  {
      $this->addMediaConversion('normal')->width(1170);
      $this->addMediaConversion('normal-webp')->width(1170)->format('webp');
      $this->addMediaConversion('double')->width(2340);
      $this->addMediaConversion('double-webp')->width(2340)->format('webp');
      $this->addMediaConversion('thumbnail')->crop('crop-center', 400, 400);
      $this->addMediaConversion('featured')->width(500)->crop('crop-center', 500, 348);
      $this->addMediaConversion('featured-webp')->width(500)->crop('crop-center', 500, 348)->format('webp');
  }

  public function registerMediaCollections()
  {
      $this->addMediaCollection('tours')->singleFile();
  }

  public function category(){
    return $this->belongsTo('App\Category');
  }

  public function events(){
    return $this->hasMany('App\Event');
  }

  public function pictures(){
    return $this->hasMany('App\Picture');
  }

}
