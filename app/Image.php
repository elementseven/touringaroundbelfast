<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Image extends Model implements HasMedia
{
	use HasMediaTrait;

	public function registerMediaConversions(Media $media = null)
	{
		$this->addMediaConversion('double')->width(1200);
	}
}

