<?php

namespace App\Http\Controllers;
use TimeHunter\LaravelGoogleReCaptchaV3\Validations\GoogleReCaptchaV3ValidationRule;
use Illuminate\Http\Request;
use App\Interest;
use App\User;

use Newsletter;
use Mail;
use Auth;
use Carbon\Carbon;

class SendMail extends Controller
{

	public function enquiry(Request $request){

    	// Validate the form data
		$this->validate($request,[
			'gRecaptchaResponse' => [new GoogleReCaptchaV3ValidationRule('contact_us')],
			'name' => 'required|string|max:255',
			'email' => 'required|email',
			'message' => 'required|string',
		]); 

		$phone = null;
		$subject = "General Enquiry";
		$name = $request->input('name');
		$email = $request->input('email');
		$content = $request->input('message');
		if($request->input('phone')){
			$phone = $request->input('phone');
		}
		Mail::send('emails.enquiry',[
			'name' => $name,
			'phone' => $phone,
			'subject' => $subject,
			'email' => $email,
			'content' => $content
		], function ($message) use ($subject, $email, $phone, $name, $content){
			$message->from('donotreply@touringaroundbelfast.com', 'Touring Around Belfast');
			$message->subject($subject);
			$message->replyTo($email);
			// $message->to('hello@elementseven.co');
			$message->to('billy@touringaroundbelfast.com');
		});
		return 'success';
	}

	public function tour(Request $request){

    	// Validate the form data
		$this->validate($request,[
			'gRecaptchaResponse' => [new GoogleReCaptchaV3ValidationRule('contact_us')],
			'name' => 'required|string|max:255',
			'email' => 'required|email',
			'date' => 'required|string',
			'tour' => 'required|string',
		]); 
		$date = Carbon::parse($request->input('date'))->format('d/m/Y');
		$phone = null;
		$subject = "Tour Request";
		$name = $request->input('name');
		$email = $request->input('email');
		$tour = $request->input('tour');
		$content = $request->input('date');
		if($request->input('phone')){
			$phone = $request->input('phone');
		}
		Mail::send('emails.tour',[
			'name' => $name,
			'tour' => $tour,
			'phone' => $phone,
			'subject' => $subject,
			'email' => $email,
			'date' => $date
		], function ($message) use ($subject, $email, $phone, $name, $tour, $date){
			$message->from('donotreply@touringaroundbelfast.com', 'Touring Around Belfast');
			$message->subject($subject);
			$message->replyTo($email);
			// $message->to('hello@elementseven.co');
			$message->to('billy@touringaroundbelfast.com');
		});
		return 'success';
	}


}
