<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Event;
use App\Tour;

class EventController extends Controller
{
    public function getByCategory($category){
        $id = $category;
        $events = Event::whereHas('tour', function($q) use($category){
           $q->where('category_id', $category)->with('category');
        })
        ->with(array('tour'=>function($query) use($id){
            $query->select('id','title','slug','excerpt','category_id')->with('category');
        }))
        ->whereDate('start', '>=', Carbon::today())
        ->orderBy('start')
        ->paginate(2);
        return $events;
    }

    // Return news as an array
    public function get(Request $request, $tour){
        $limit = 6;
        if($tour != 0){
            $tour = Tour::find($tour);
            if($request->input('start')){
                $start = Carbon::createFromFormat('Y-m-d\TH:i:s.uP', $request->input('start'));
                $events = Event::where('tour_id',$tour->id)
                ->whereDate('start', $start)
                ->orderBy('start','asc')
                ->paginate($limit);
            }else{
                $events = Event::where('tour_id',$tour->id)
                ->whereDate('start', '>=', Carbon::today())
                ->orderBy('start','asc')
                ->paginate($limit);
            }
        }else{
            if($request->input('start')){
                $start = Carbon::createFromFormat('Y-m-d\TH:i:s.uP', $request->input('start'));
                $events = Event::whereDate('start', $start)
                ->with(array('tour'=>function($query){
                    $query->select('id','title','slug','excerpt','category_id')->with('category');
                }))
                
                ->orderBy('start','asc')
                ->paginate($limit);
            }else{
                $events = Event::whereDate('start', '>=', Carbon::today())
                ->with(array('tour'=>function($query){
                    $query->select('id','title','slug','excerpt','category_id')->with('category');
                }))
                ->orderBy('start','asc')
                ->paginate($limit);
            }
        }
        
        
        
        return $events;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tour $tour)
    {
        return view('admin.tours.events')->with(['tour' => $tour]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'link' => 'required|string|max:255',
            'start' => 'required|string|max:255',
            'tour' => 'required'
        ]);
        $start = Carbon::createFromFormat('Y-m-d\TH:i:s.uP', $request->input('start'));
        $event = Event::create([
            'link' => $request->input('link'),
            'start' => $start,
            'tour_id'=> $request->input('tour')
        ]);
        return $event;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();
        return "success";
    }
}
