<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Testimonial;
use App\Tour;
use App\Post;

class PageController extends Controller
{
  public function welcome(Request $request){
    $tours = Tour::where('status','Featured')->orderBy('created_at','desc')->paginate(3);
    return view('welcome', compact('tours'));
  }

  public function contact(){
    return view('contact');
  }

  public function about(){
    $tours = Tour::where('status','Featured')->orderBy('created_at','desc')->paginate(3);
    return view('about', compact('tours'));
  }

  public function testimonials(){
    $testimonials = Testimonial::where('status','Published')->orderBy('date','desc')->get();
    return view('testimonials')->with(['testimonials' => $testimonials]);
  }

  public function tours(){
    $tours = Tour::where('status','!=','Draft')->orderBy('created_at','desc')->get();
    return view('tours.index')->with(['tours' => $tours]);
  }

  public function tourShow(Request $request, $slug){
  	$tour = Tour::where('slug', $slug)->first();
    if ($tour && $request->session()->has($tour->slug)) {
      $tour->liked = true;
    }else{
      $tour->liked = false;
    }
    return view('tours.show')->with(['tour' => $tour]);
  }

}
