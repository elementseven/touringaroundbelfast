<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Image;
use App\Tour;

class TourController extends Controller
{

    public function like(Request $request, Tour $tour){
        if (!$request->session()->has($tour->slug)) {
            $tour->likes++;
            $tour->save();
            $request->session()->put($tour->slug, true);
            $request->session()->save();
        }
        return $tour;
    }
    public function unlike(Request $request, Tour $tour){
        if ($request->session()->has($tour->slug)) {
            $tour->likes--;
            $tour->save();
            $request->session()->forget($tour->slug);
            $request->session()->save();
        }
        return $tour;
    }

    // Return news as an array
    public function getTours(Request $request, $category, $limit){
        $search = $request->input('search');
        if($category == 'all'){
            $tours = Tour::where('title','LIKE','%'.$search.'%')
            ->orderBy('created_at','desc')
            ->with('category')
            ->paginate($limit);
        }else{
            $tours = Tour::orderBy('created_at','desc')->whereHas('category', function($q) use($category){
                $q->where('slug', $category);
            })
            ->where('title','LIKE','%'.$search.'%')
            ->orderBy('created_at','desc')
            ->with('category')
            ->paginate($limit);
        }
        foreach($tours as $t){
            $t->image = $t->getFirstMediaUrl('featured');
        }
        return $tours;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tours.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('relates_to', 'tours')->get();
        return view('admin.tours.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'starts_at' => 'required|string|max:255',
            'excerpt' => 'required',
            'status' => 'required',
            'category' => 'required',
            'description' => 'required',
            'description2' => 'required',
            'locations' => 'required',
            'featured' => 'required',
            'image1' => 'required',
            'image2' => 'required',
        ]);
        if (
            $request->hasFile('featured') && 
            $request->file('featured')->isValid() && 
            $request->file('featured')->getClientOriginalName() != "" &&
            $request->hasFile('image1') && 
            $request->file('image1')->isValid() && 
            $request->file('image1')->getClientOriginalName() != "" &&
            $request->hasFile('image2') && 
            $request->file('image2')->isValid() && 
            $request->file('image2')->getClientOriginalName() != ""
        ){
            $tour = Tour::create([
                'title' => $request->input('title'),
                'starts_at' => $request->input('starts_at'),
                'excerpt'=> $request->input('excerpt'),
                'status'=> $request->input('status'),
                'category_id'=> $request->input('category'),
                'description' => $request->input('description'),
                'description2' => $request->input('description2'),
                'locations' => $request->input('locations'),
                'link' => $request->input('link'),
                'likes' => 0
            ]);
            $tour->addMediaFromRequest('featured')->toMediaCollection('featured', 'media');
            $tour->addMediaFromRequest('image1')->toMediaCollection('image1', 'media');
            $tour->addMediaFromRequest('image2')->toMediaCollection('image2', 'media');

            return $tour;
        }
        
    }

    // Function to handle uploading images in the body
    public function imageUpload(Request $request)
    {
        $this->validate($request,[
            'img' => 'required'
        ]);
        $img = Image::create([]);
        if ($request->hasFile('img') && $request->file('img')->isValid() && $request->file('img')->getClientOriginalName() != "") {
            $img->addMediaFromRequest('img')->toMediaCollection('featured', 'media');
        }
        $media = $img->getFirstMediaUrl('featured','double');
        return $media;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tour $tour)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour $tour)
    {
        $categories = Category::where('relates_to', 'tours')->get();
        $tour = Tour::where('id', $tour->id)->with('category')->first();
        return view('admin.tours.edit', compact('tour','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour $tour)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'starts_at' => 'required|string|max:255',
            'excerpt' => 'required',
            'status' => 'required',
            'category' => 'required',
            'description' => 'required',
            'description2' => 'required',
            'locations' => 'required'
        ]);

        if ($request->hasFile('featured') && 
            $request->file('featured')->isValid() && 
            $request->file('featured')->getClientOriginalName() != "") 
        {
            $media = $tour->getFirstMedia('featured');
            if($media){
                $media->delete();
            }
            $tour->addMediaFromRequest('featured')->toMediaCollection('featured', 'media');
        }

        if ($request->hasFile('image1') && 
            $request->file('image1')->isValid() && 
            $request->file('image1')->getClientOriginalName() != "") 
        {
            $media = $tour->getFirstMedia('image1');
            if($media){
                $media->delete();
            }
            $tour->addMediaFromRequest('image1')->toMediaCollection('image1', 'media');
        }

        if ($request->hasFile('image2') && 
            $request->file('image2')->isValid() && 
            $request->file('image2')->getClientOriginalName() != "") 
        {
            $media = $tour->getFirstMedia('image2');
            if($media){
                $media->delete();
            }
            $tour->addMediaFromRequest('image2')->toMediaCollection('image2', 'media');
        }

        $update = [
            'title' => $request->input('title'),
            'starts_at' => $request->input('starts_at'),
            'excerpt'=> $request->input('excerpt'),
            'status'=> $request->input('status'),
            'category_id'=> $request->input('category'),
            'description' => $request->input('description'),
            'description2' => $request->input('description2'),
            'locations' => $request->input('locations'),
            'link' => $request->input('link')
        ];

        $tour->update($update);

        return $tour;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour $tour)
    {
        $tour->media->each->delete();
        $tour->delete();
        return "success";
    }
}
