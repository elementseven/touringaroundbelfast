<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Picture;
use App\Tour;

class PictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Tour $tour)
    {
        $pictures = Picture::where('tour_id', $tour->id)->orderBy('order')->get();
        foreach($pictures as $p){
            $p->image = $p->getFirstMediaUrl('pictures');
        }
        return view('admin.tours.pictures', compact('tour','pictures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Tour $tour)
    {
        $this->validate($request,[
            'size' => 'required|numeric|max:12',
            'order' => 'required|numeric'
        ]);
        if ($request->hasFile('image') && $request->file('image')->isValid() && $request->file('image')->getClientOriginalName() != "") {
            $picture = Picture::create([
                'size' => $request->input('size'),
                'order'=> $request->input('order'),
                'tour_id'=> $tour->id
            ]);
            $picture->addMediaFromRequest('image')->toMediaCollection('pictures', 'media');

        }else if($request->input('video')){
            $picture = Picture::create([
                'size' => $request->input('size'),
                'order'=> $request->input('order'),
                'video_link'=> $request->input('video'),
                'tour_id'=> $tour->id
            ]);
        }
        return $picture;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour $tour)
    {
        $this->validate($request,[
            'images' => 'required'
        ]);
        foreach(json_decode($request->input('images')) as $img){
            $picture = Picture::where('id',$img->id)->first();
            $picture->order = $img->order;
            $picture->save();
        }
        return "success";
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Picture $picture)
    {
        $tour = $picture->tour;
        $picture->media->each->delete();
        $picture->delete();
        $pictures = Picture::where('tour_id', $tour->id)->orderBy('order')->get();
        foreach($pictures as $key => $p){
            $p->order = $key + 1;
            $p->save();
        }
        return "success";
    }
}
