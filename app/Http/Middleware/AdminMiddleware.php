<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() != NULL && $request->user()->role->id == 1){
            return $next($request);
        }else{
            Auth::logout();
            return redirect('login');
        }
    }
}
