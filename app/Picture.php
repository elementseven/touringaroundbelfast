<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Picture extends Model implements HasMedia
{
  use HasMediaTrait;

 	protected $fillable = [
	  'size','photo','order','tour_id'
	];

	public function registerMediaConversions(Media $media = null)
  {
  	$this->addMediaConversion('square')->crop('crop-center', 500, 500);
    $this->addMediaConversion('square-webp')->crop('crop-center', 500, 500)->format('webp');
  }

  public function registerMediaCollections()
  {
    $this->addMediaCollection('pictures')->singleFile();
  }

  public function tour(){
    return $this->belongsTo('App\Tour');
  }
}
