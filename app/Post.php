<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Post extends Model implements HasMedia
{
	use HasMediaTrait;

    protected $fillable = [
        'title','slug','status','excerpt','body','category_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $slug = strtolower($post->title);
            //Make alphanumeric (removes all other characters)
            $slug = preg_replace("/[^a-z0-9_\s-]/", "", $slug);
            //Clean up multiple dashes or whitespaces
            $slug = preg_replace("/[\s-]+/", " ", $slug);
            //Convert whitespaces and underscore to dash
            $slug = preg_replace("/[\s_]/", "-", $slug);

            $post->slug = $slug;
        });
    }

    public function registerMediaConversions(Media $media = null)
    {
    	$this->addMediaConversion('normal')->crop('crop-center', 376, 261);
      $this->addMediaConversion('normal-webp')->crop('crop-center', 376, 261)->format('webp');
      $this->addMediaConversion('double')->crop('crop-center', 752, 522);
      $this->addMediaConversion('double-webp')->crop('crop-center', 752, 522)->format('webp');
      $this->addMediaConversion('mobile')->crop('crop-center', 300, 208);
      $this->addMediaConversion('mobile-webp')->crop('crop-center', 300, 208)->format('webp');
    }
    public function category(){
      return $this->belongsTo('App\Category');
    }
}