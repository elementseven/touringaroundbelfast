<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{

	protected $fillable = [
    'name','date','content','status','link_url','link_text'
  ];

  //Casts of the model dates
	protected $casts = [
	    'date' => 'date'
	];
}
