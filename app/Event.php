<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $fillable = [
    'link','start','status','tour_id'
  ];

  public function tour(){
    return $this->belongsTo('App\Tour');
  }
}
