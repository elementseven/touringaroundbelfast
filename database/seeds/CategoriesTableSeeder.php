<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Category::create([
        'name' => 'Hidden Tours',
        'relates_to' => 'tours'
	    ]);
	    Category::create([
        'name' => 'Food & Drink',
        'relates_to' => 'tours'
	    ]);
	    Category::create([
        'name' => 'Culture',
        'relates_to' => 'tours'
	    ]);
	    Category::create([
        'name' => 'Experiences',
        'relates_to' => 'tours'
	    ]);
	    Category::create([
        'name' => 'Game of Thrones',
        'relates_to' => 'tours'
	    ]);
	    Category::create([
        'name' => 'Advice',
        'relates_to' => 'posts'
	    ]);
	    Category::create([
        'name' => 'Announcement',
        'relates_to' => 'posts'
	    ]);
	    Category::create([
        'name' => 'Press Release',
        'relates_to' => 'posts'
	    ]);
	    Category::create([
        'name' => 'Information',
        'relates_to' => 'posts'
	    ]);
    }
}
